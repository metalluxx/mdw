//
//  NetworkManagerTest.swift
//  MDWTests
//
//  Created by Metalluxx on 07/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import XCTest
@testable import MDW

class NetworkManagerTest:XCTestCase {

    var networkManager:NetworkManager!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        networkManager = NetworkManager()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        networkManager = nil
        super.tearDown()
    }
    
    
    func test_GetKey() -> Void {
            
            let expectation = XCTestExpectation(description:"Get key for auth")
            
            networkManager.getKey(phone:"79008007999") { (tKey, tErr) in
                XCTAssertNil(tErr)
                XCTAssertNotNil(tKey)
                
                if let tErr = tErr {
                    XCTFail(tErr)
                    expectation.fulfill()
                    return
                }
                guard let _ = tKey else {
                    XCTFail("Error unswaping key from data")
                    expectation.fulfill()
                    return
                }
                expectation.fulfill()
                
            }
            
            wait(for:[expectation], timeout:15.0)
            
        
    }

    
    func testDownloadImage() -> Void {
        
        let expectation = XCTestExpectation()
        
        networkManager.downloadImage(urlPath: "https://hsto.org/r/w1560/webt/3s/ap/0w/3sap0w70jyzre12wi-c8r9132l4.png") { (image, err) in
            print(image)
            XCTAssertNotNil(image, "Image downloaded successful")
            XCTAssertNil(err, "Dont have errors before downloading image")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 15.0)
    }
    
    
    
//    func testExample() {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
//
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
