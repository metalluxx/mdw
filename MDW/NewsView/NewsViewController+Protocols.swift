//
//  NewsViewController+SVDelegate.swift
//  MDW
//
//  Created by Metalluxx on 31/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit


extension NewsViewController:UITableViewDataSource {
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return material?.blocks.count ?? 0
    }
    
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        
        if material!.blocks[indexPath.row].type == .image {
            let imageParagraph = tableView.dequeueReusableCell(withIdentifier:NewsImageParagraph.reuseIdentifier) as! NewsImageParagraph
            imageParagraph.imageCover = material!.blocks[indexPath.row].content.image
            imageParagraph.configureCell()
            return imageParagraph
        }
            
        else if material!.blocks[indexPath.row].type == .paragraph {
            let paragraph = tableView.dequeueReusableCell(withIdentifier:NewsTextParagraph.reuseIdentifier) as! NewsTextParagraph
            paragraph.block = material!.blocks[indexPath.row]
            paragraph.configureCell()
            return paragraph
        }
        
        return UITableViewCell()
    }
    
    
}

extension NewsViewController:UITableViewDelegate {
    
    func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath) -> CGFloat {
        if material!.blocks[indexPath.row].type == .image {
            return MDWConstant.NewsImageCellHeight
        }
        return UITableView.automaticDimension
    }
}


extension NewsViewController:ScrollViewOffsetDelegate{
    func processOffsetFromScrollView(point:CGPoint) {
        
        isTransperent = (-point.y > (navBar.frame.origin.y + navBar.frame.size.height) )
        
        if (-point.y) >= 0
        {
            imageHeightConstraint.constant = -point.y 
        }
    }

}
