//
//  NewsImageParagraph.swift
//  MDW
//
//  Created by Metalluxx on 29/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class NewsImageParagraph:UITableViewCell, MDWImageCacheProtocol {
    static var reuseIdentifier = "NEWS_IMAGE_CELL"
    var cache = NSCache<NSString, UIImage>()
    
    private var imageParagraph:UIImageView!
    var imageCover:MDWImage?
    
    override init(style:UITableViewCell.CellStyle, reuseIdentifier:String?) {
        super.init(style:style, reuseIdentifier:reuseIdentifier)
        self.selectionStyle = .none
        imageParagraph = UIImageView(image:imageCover?.getPreviewImage)
        
        imageParagraph.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(imageParagraph)
        
        NSLayoutConstraint.activate([
                imageParagraph.topAnchor.constraint(equalTo:contentView.topAnchor),
                imageParagraph.bottomAnchor.constraint(equalTo:contentView.bottomAnchor),
                imageParagraph.leftAnchor.constraint(equalTo:contentView.leftAnchor),
                imageParagraph.rightAnchor.constraint(equalTo:contentView.rightAnchor),
            ])
    }
    
    func configureCell() -> Void {
        guard let imageCover = self.imageCover else {return}
        
        if let imageFromDataSource = getImageFromCache(string: imageCover.original) {
            DispatchQueue.main.async {
                self.imageParagraph.image = imageFromDataSource
            }
        }
        else{
            DispatchQueue.main.async {
                self.imageParagraph.image = imageCover.getPreviewImage
            }
            NetworkManager.shared.downloadImage(urlPath: imageCover.original, completion: { (image, error) in
                guard let image = image else {return}
                DispatchQueue.main.async {
                    self.imageParagraph.image = image
                }
                self.storeImageInCache(image: image, string: imageCover.original)
            })
        }
    }
    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
    }
}
