//
//  NewsViewController.swift
//  MDW
//
//  Created by Metalluxx on 21/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class NewsViewController:CoordinatorViewController, MDWImageCacheProtocol {
    weak var manageCoordinator: BaseCoordinator?
    var cache = NSCache<NSString, UIImage>()
    deinit {
        manageCoordinator?.finishFlow?()
    }
    var isTransperent:Bool = true {
        didSet{
            let textAttributes = [NSAttributedString.Key.foregroundColor:(isTransperent ? UIColor.clear:UIColor.black)]
            UIView.animate(withDuration:0.1) {
                self.navBar.titleTextAttributes = textAttributes
                self.backButton.tintColor = (self.isTransperent ? .white:UIColor(red:0.6, green:0.6, blue:0.6, alpha:1))
            }
            UIView.animate(withDuration:0.3) {
                self.statusBarView.backgroundColor = (self.isTransperent ? .clear:.white)
            }
        }
    }
    
    var tableView = NewsTableView(frame:.zero, style:.plain)
    
    var imageHeader = NewsHeaderView()
    var imageHeightConstraint:NSLayoutConstraint!
    
    var navBar = UINavigationBar()
    var statusBarView = UIView()
    var navBarItem:UINavigationItem!
    var backButton:UIBarButtonItem!

    var material:MDWMaterial?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        tableView.register(NewsImageParagraph.self, forCellReuseIdentifier:NewsImageParagraph.reuseIdentifier)
        tableView.register(NewsTextParagraph.self, forCellReuseIdentifier:NewsTextParagraph.reuseIdentifier)
        
        //MARK:- NavBar
        
        navBarItem = UINavigationItem(title:material?.title ?? "")
        navBar.setItems([navBarItem], animated:true)
        
        navBar.isTranslucent = true
        
        backButton = UIBarButtonItem(title:NSLocalizedString("BACK_NAVBAR", comment:""), style:.plain, target:navigationController, action:#selector(UINavigationController.popViewController(animated:)))
        navBarItem.setLeftBarButton(backButton, animated:true)

        navBar.setBackgroundImage(UIImage(), for:.default)
        navBar.shadowImage = UIImage()
        
        imageHeader.titleText = material?.title
        imageHeader.typeText = material?.type.rawValue
        
        //MARK:  TableView
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        tableView.dataSource = self
        tableView.offsetDelegate = self
        tableView.delegate = self
        tableView.separatorColor = .clear
        tableView.layoutMargins = .zero
        
        navBar.translatesAutoresizingMaskIntoConstraints = false
        statusBarView.translatesAutoresizingMaskIntoConstraints = false
        imageHeader.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(navBar)
        view.insertSubview(statusBarView, belowSubview:navBar)
        view.insertSubview(imageHeader, at:0)
        view.insertSubview(tableView, belowSubview:statusBarView)
        
        imageHeightConstraint = imageHeader.heightAnchor.constraint(equalToConstant:MDWConstant.ImageHeader.height)
        
        NSLayoutConstraint.activate([
            
            navBar.topAnchor.constraint(equalTo:view.layoutMarginsGuide.topAnchor),
            navBar.leftAnchor.constraint(equalTo:view.leftAnchor),
            navBar.rightAnchor.constraint(equalTo:view.rightAnchor),
            
            statusBarView.topAnchor.constraint(equalTo:view.topAnchor),
            statusBarView.leftAnchor.constraint(equalTo:view.leftAnchor),
            statusBarView.rightAnchor.constraint(equalTo:view.rightAnchor),
            statusBarView.bottomAnchor.constraint(equalTo:navBar.bottomAnchor),
            
            imageHeightConstraint,
            imageHeader.topAnchor.constraint(equalTo:view.topAnchor),
            imageHeader.leftAnchor.constraint(equalTo:view.leftAnchor),
            imageHeader.rightAnchor.constraint(equalTo:view.rightAnchor),
            
            tableView.topAnchor.constraint(equalTo:view.topAnchor),
            tableView.leftAnchor.constraint(equalTo:view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo:view.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo:view.layoutMarginsGuide.bottomAnchor),
            
            ])
   
        view.layoutSubviews()
        
        tableView.contentInset = UIEdgeInsets(top:MDWConstant.ImageHeader.height + statusBarView.frame.height, left:0, bottom:0, right:0)
        imageHeader.defaultHeigth = tableView.contentInset.top
        
        downloadImageForTitle()
        tableView.reloadData()
    }
    

    func downloadImageForTitle() -> Void {
        
        imageHeader.image = material?.fullCover.getPreviewImage

        if let imageUrl = material?.fullCover.original{
            
            if let unswapImage = getImageFromCache(string: imageUrl){
                DispatchQueue.main.async {
                    self.imageHeader.image = unswapImage
                }
            } else {
                NetworkManager.shared.downloadImage(urlPath: imageUrl, completion: { (image, error) in
                    if error == nil {
                        if let image:UIImage = image {
                            DispatchQueue.main.async {
                                self.imageHeader.image = image
                            }
                            self.storeImageInCache(image: image, string: imageUrl)
                        }
                    }
                })
            }
        }
    }
    
    
    convenience init(material:MDWMaterial) {
        self.init()
        self.material = material
    }
    
    override func viewWillAppear(_ animated:Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
}


