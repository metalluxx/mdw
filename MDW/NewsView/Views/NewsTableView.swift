//
//  NewsTableView.swift
//  MDW
//
//  Created by Metalluxx on 31/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

protocol ScrollViewOffsetDelegate:NSObjectProtocol {
    func processOffsetFromScrollView(point:CGPoint) -> Void
}

class NewsTableView:UITableView {
    weak var offsetDelegate:ScrollViewOffsetDelegate?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        offsetDelegate?.processOffsetFromScrollView(point:contentOffset)
    }
}
