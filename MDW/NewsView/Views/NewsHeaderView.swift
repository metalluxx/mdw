//
//  NewsHeaderView.swift
//  MDW
//
//  Created by Metalluxx on 01/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class NewsHeaderView:UIView {
    private var imageView = UIImageView()
    private var titleLabel = UILabel()
    private var typeLabel = UILabel()

    var defaultHeigth = CGFloat(0.0)
    
    var titleText:String? {
        set{ titleLabel.text = newValue }
        get{ return titleLabel.text }
    }
    var typeText:String? {
        set{ typeLabel.text = newValue }
        get{ return typeLabel.text }
    }
    var image:UIImage? {
        get{ return imageView.image }
        set{ imageView.image = newValue }
    }
    
    override func draw(_ rect:CGRect) {
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        titleLabel.textColor = MDWConstant.ImageHeader.textColor
        typeLabel.textColor = MDWConstant.ImageHeader.textColor

        titleLabel.layer.shadowColor = UIColor.black.cgColor
        titleLabel.layer.shadowRadius = 6
        titleLabel.layer.shadowOpacity = 1
        
        typeLabel.layer.shadowColor = UIColor.black.cgColor
        typeLabel.layer.shadowRadius = 4
        typeLabel.layer.shadowOpacity = 1
        
        titleLabel.font = MDWConstant.boldFont(ofSize:24)
        typeLabel.font = MDWConstant.normalFont(ofSize:17)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        typeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        insertSubview(imageView, at: 0)
        insertSubview(titleLabel, at: 1)
        insertSubview(typeLabel, at: 1)
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo:topAnchor),
            imageView.bottomAnchor.constraint(equalTo:bottomAnchor),
            imageView.leftAnchor.constraint(equalTo:leftAnchor),
            imageView.rightAnchor.constraint(equalTo:rightAnchor),
            
            typeLabel.bottomAnchor.constraint(equalTo:bottomAnchor, constant:-20),
            typeLabel.leftAnchor.constraint(equalTo:leftAnchor, constant:20),
            typeLabel.rightAnchor.constraint(equalTo:rightAnchor, constant:-20),
            typeLabel.heightAnchor.constraint(equalToConstant:17),
            
            titleLabel.bottomAnchor.constraint(equalTo:typeLabel.topAnchor, constant:-10),
            titleLabel.leftAnchor.constraint(equalTo:leftAnchor, constant:20),
            titleLabel.rightAnchor.constraint(equalTo:rightAnchor, constant:-20),
            titleLabel.heightAnchor.constraint(equalToConstant:24),
            ])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let offsetHeigth =  frame.height / defaultHeigth
        titleLabel.textColor = titleLabel.textColor.withAlphaComponent(offsetHeigth)
        typeLabel.textColor = typeLabel.textColor.withAlphaComponent(offsetHeigth)

    }
}
