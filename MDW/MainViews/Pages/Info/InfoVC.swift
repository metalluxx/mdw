//
//  InfoVC.swift
//  MDW
//
//  Created by Metalluxx on 17/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class InfoVC:CoordinatorViewController,MDWTabBarItemContainer{
    var mdwTabBarItem = MDWTabBarItemField(image:UIImage(named:"tabInfo"), selectedImage:nil, title:NSLocalizedString("INFO_TB", comment:""))
    weak var manageCoordinator: BaseCoordinator?
    
    private var scrollView = UIScrollView()
    private var navBarView:MDWNavBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() -> Void {
        navBarView = MDWNavBarWithButton(viewController:self, title:NSLocalizedString("INFO_FULL_TB", comment:""), buttonText:(SessionManager.shared.sessionToken == nil ? "Login":"Logout"), target:self, selector:#selector(logoutSelector))
        
        view.backgroundColor = UIColor.white
        
        scrollView.delegate = self
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo:navBarView.bottomAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo:view.bottomAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo:view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo:view.rightAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true

        
        let button1 = MDWLargeButton()
        button1.titleText = NSLocalizedString("INF_NEWS", comment:"")
        button1.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(button1)
        button1.topAnchor.constraint(equalTo:scrollView.topAnchor, constant:10).isActive = true
        button1.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:10).isActive = true
        button1.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-10).isActive = true
        button1.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -20.0).isActive = true
        button1.heightAnchor.constraint(equalToConstant:115).isActive = true
        
        
        let button2 = MDWLargeButton()
        button2.titleText = NSLocalizedString("INF_PARTICIPANTS", comment:"")
        button2.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(button2)
        button2.topAnchor.constraint(equalTo:button1.bottomAnchor, constant:10).isActive = true
        button2.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:10).isActive = true
        button2.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-10).isActive = true
        button2.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -20.0).isActive = true
        button2.heightAnchor.constraint(equalToConstant:95).isActive = true
        
        let button3 = MDWLargeButton()
        button3.titleText = NSLocalizedString("INF_PARTNERS", comment:"")
        button3.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(button3)
        button3.topAnchor.constraint(equalTo:button2.bottomAnchor, constant:10).isActive = true
        button3.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:10).isActive = true
        button3.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-10).isActive = true
        button3.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -20.0).isActive = true
        button3.heightAnchor.constraint(equalToConstant:75).isActive = true
        
        let button4 = MDWLargeButton()
        button4.titleText = NSLocalizedString("INF_CONTACTS", comment:"")
        button4.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(button4)
        button4.topAnchor.constraint(equalTo:button3.bottomAnchor, constant:10).isActive = true
        button4.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:10).isActive = true
        button4.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-10).isActive = true
        button4.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -20.0).isActive = true
        button4.heightAnchor.constraint(equalToConstant:75).isActive = true
        
        let button5 = MDWLargeButton()
        button5.titleText = NSLocalizedString("INF_RATEAPP", comment:"")
        button5.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(button5)
        button5.topAnchor.constraint(equalTo:button4.bottomAnchor, constant:10).isActive = true
        button5.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:10).isActive = true
        button5.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-10).isActive = true
        button5.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -20.0).isActive = true
        button5.heightAnchor.constraint(equalToConstant:40).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: button5.bottomAnchor, constant: 20).isActive = true
        
    }
    
    @objc func logoutSelector() -> Void {
        
        guard SessionManager.shared.sessionToken != nil else {
            SessionManager.shared.sessionStatus = .none
            self.manageCoordinator?.finishFlow?()
            return
        }
        
        let alert = UIAlertController(title:NSLocalizedString("WARN", comment:""), message:NSLocalizedString("INF_ACCEPT_LEAVE", comment:"") , preferredStyle:UIAlertController.Style.actionSheet)
        
        alert.addAction(UIAlertAction(title:NSLocalizedString("YES", comment:""),
                                           style:UIAlertAction.Style.destructive,
                                           handler:{ (alert) in
                                            SessionManager.shared.sessionToken = nil
                                            SessionManager.shared.sessionStatus = .none
                                            self.manageCoordinator?.finishFlow?()
                                            
        }))
        alert.addAction(UIAlertAction(title:NSLocalizedString("NO", comment:""), style:UIAlertAction.Style.cancel, handler:nil ))
        present(alert, animated:true)
    }
    
}


extension InfoVC:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView:UIScrollView) {
        navBarView.isSeparated = scrollView.contentOffset.y > 10
    }
}
