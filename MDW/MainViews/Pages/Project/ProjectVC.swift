//
//  FormiqueVC.swift
//  MDW
//
//  Created by Metalluxx on 17/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class ProjectVC:CoordinatorViewController,MDWTabBarItemContainer{
    var mdwTabBarItem = MDWTabBarItemField(image: UIImage(named:"tabProj"), selectedImage: nil, title: "Project")
    weak var manageCoordinator: BaseCoordinator?
    
    var tableView = MDWTableView()
    var refreshControl = UIRefreshControl()
    var tableDataSource = ProjectDataSource()
    
    var errorView:ErrorAlertNotificationView?
    var navBarView:MDWNavBar!
    
    var _tabBarItem = UITabBarItem(title:NSLocalizedString("PRJ_TB", comment:""), image:UIImage(named:"tabProj"), tag:0)
    
    override var tabBarItem:UITabBarItem!{
        get{
            let item = _tabBarItem
            item.titlePositionAdjustment = MDWTabController.itemOffset
            return item
        }
        set{ _tabBarItem = newValue }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadNews()
    }
    
    func setupUI() -> Void {
        tableView.register(ProjectTableCell.self, forCellReuseIdentifier:ProjectTableCell.reuseIdentifier)
        navBarView = MDWNavBar(viewController:self, title:NSLocalizedString("PRJ_TB", comment:""))
        view.backgroundColor = .white

        refreshControl.addTarget(self, action:#selector(loadNews), for:UIControl.Event.valueChanged)
        refreshControl.tintColor = MDWConstant.NavigationBarColors.title
        refreshControl.attributedTitle = NSAttributedString.init(
            string:NSLocalizedString("PRJ_LOADING_NEWS", comment:""),
            attributes:[NSAttributedString.Key.font:MDWConstant.normalFont(ofSize:15)])
        
        tableView.delegate = self
        tableView.dataSource = tableDataSource
        tableView.refreshControl = self.refreshControl
        tableView.allowsSelection = true
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo:navBarView.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo:view.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo:view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo:view.rightAnchor),
            ]) 
    }
    
    @objc func loadNews () -> Void  {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        NetworkManager.shared.downloadShortMaterials(completion:{ (arrayNews, err) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.refreshControl.endRefreshing()
                guard let unswapArrayNews = arrayNews else {
                    self.errorView?.removeFromSuperview()
                    self.errorView = ErrorAlertNotificationView.showingAlert(in:self.view, info:err ?? NSLocalizedString("ERROR", comment:""), type:.error)
                    return
                }
                self.tableDataSource.arrayNews = unswapArrayNews
                self.tableView.reloadData()
            }
        })
    }
}

extension ProjectVC:UITableViewDelegate {
    
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath) {
        let material = tableDataSource.arrayNews[indexPath.row]
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        DispatchQueue.main.async {
            self.tableView.allowsSelection = false
        }
        
        NetworkManager.shared.downloadFullMaterial(shortMaterial:material, completion:{ (fullMaterial, err) in
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.tableView.allowsSelection = true
                
                if let error = err {
                    self.errorView?.removeFromSuperview()
                    self.errorView = ErrorAlertNotificationView.showingAlert(in:self.view, info:error, type:.error)
                    return
                }
                
                guard let unswapFullMaterial = fullMaterial else {
                    self.errorView?.removeFromSuperview()
                    self.errorView = ErrorAlertNotificationView.showingAlert(in:self.view, info:NetworkResponse.unableToDecode.rawValue, type:.error)
                    return
                }
                guard let manageCoordinator = self.manageCoordinator else {
                    self.errorView?.removeFromSuperview()
                    self.errorView = ErrorAlertNotificationView.showingAlert(in:self.view, info:"Dont have router in coordinator", type:.error)
                    return
                }
                CoordinatorFactory.createNewsCoordinator(parentCoordinator:manageCoordinator, news:unswapFullMaterial).start()
            }
        })
    }
    
    func scrollViewDidScroll(_ scrollView:UIScrollView) {
        navBarView.isSeparated = (tableView.contentOffset.y > MDWConstant.OffsetForShowingSeparator)
    }
    
    func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath) -> CGFloat {
        return MDWConstant.ProjectTableCellHeigth
    }
    
}



