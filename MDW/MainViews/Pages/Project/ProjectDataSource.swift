//
//  FormiqueDataSource.swift
//  MDW
//
//  Created by Metalluxx on 18/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class ProjectDataSource:NSObject,UITableViewDataSource {

    public var arrayNews = [MDWShortMaterial]()

    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrayNews.count
    }
    
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:ProjectTableCell.reuseIdentifier) as! ProjectTableCell
        cell.material = arrayNews[indexPath.row]
        cell.configureCell()
        return cell
    }
}
