//
//  MDWTableView.swift
//  MDW
//
//  Created by Metalluxx on 24/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit


class MDWTableView:UITableView{
    
    override init(frame:CGRect, style:UITableView.Style) {
        super.init(frame:frame, style:style)
        setupUI()
    }
    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
    }
    
    func setupUI() -> Void {
        allowsSelection = false
        layoutMargins = .zero
    }
    
    override func reloadData() {
        super.reloadData()
        (dataSource?.tableView(self, numberOfRowsInSection:0) == 0) ? (separatorColor = .clear):(separatorColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1))
    }
}
