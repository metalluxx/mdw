//
//  ProjectsVC.swift
//  MDW
//
//  Created by Metalluxx on 17/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class FormiqueVC:CoordinatorViewController,MDWTabBarItemContainer{
    var mdwTabBarItem = MDWTabBarItemField(image:UIImage(named:"tabForm"), selectedImage:nil, title:NSLocalizedString("FRQ_TB", comment:""))
    weak var manageCoordinator: BaseCoordinator?
    
    private var scrollView = UIScrollView()
    private var navBarView:MDWNavBar!
    
    private var textView = UITextView()
    private var tfGivenName = MDWTextFieldView()
    private var tfFamilyName = MDWTextFieldView()
    private var tfCity = MDWTextFieldView()
    private var tfAge = MDWTextFieldView()
    private var tfEducation = MDWTextFieldView()
    private var buttonSend = UIButton(type:.system)
    
    private var animationController =  MDWTextFieldScrollController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        navBarView = MDWNavBar(viewController:self, title:NSLocalizedString("FRQ_TB", comment:""))
        scrollView = UIScrollView()
        textView = UITextView()
        
        scrollView.delegate = self
        scrollView.alwaysBounceVertical = true
        scrollView.keyboardDismissMode = .onDrag
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target:self.view, action:#selector(UIView.endEditing)))

        textView.isEditable = false
        textView.isSelectable = false
        textView.font = MDWConstant.normalFont(ofSize:17)
        textView.text = NSLocalizedString("FRQ_TITLE", comment:"")
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(scrollView)
        scrollView.addSubview(textView)
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo:navBarView.bottomAnchor),
            scrollView.leftAnchor.constraint(equalTo:view.leftAnchor),
            scrollView.rightAnchor.constraint(equalTo:view.rightAnchor),
            scrollView.bottomAnchor.constraint(equalTo:view.bottomAnchor),
            
            textView.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:20),
            textView.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-20),
            textView.heightAnchor.constraint(equalToConstant:70),
            textView.topAnchor.constraint(equalTo:scrollView.topAnchor),
            textView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -40)
            ])
        
        tfGivenName.fieldText.returnKeyType = .done
        tfGivenName.fieldText.autocorrectionType = .no
        tfGivenName.labelText.text = NSLocalizedString("FRQ_GIVENNAME", comment:"")

        tfFamilyName.fieldText.autocorrectionType = .no
        tfFamilyName.fieldText.returnKeyType = .done
        tfFamilyName.labelText.text = NSLocalizedString("FRQ_FAMILYNAME", comment:"")

        tfCity.fieldText.autocorrectionType = .no
        tfCity.fieldText.returnKeyType = .done
        tfCity.labelText.text = NSLocalizedString("FRQ_CITY", comment:"")

        tfAge.fieldText.autocorrectionType = .no
        tfAge.fieldText.returnKeyType = .done
        tfAge.fieldText.keyboardType = .numberPad
        tfAge.labelText.text = NSLocalizedString("FRQ_AGE", comment:"")

        tfEducation.fieldText.autocorrectionType = .no
        tfEducation.fieldText.returnKeyType = .done
        tfEducation.labelText.text = NSLocalizedString("FRQ_EDUCATION", comment:"")

        buttonSend.addTarget(self, action:#selector(sendForm), for:.touchUpInside)
        buttonSend.backgroundColor = MDWConstant.GrayButtonBackColor
        buttonSend.setTitleColor(UIColor.black, for:.normal)
        buttonSend.titleLabel?.font = MDWConstant.normalFont(ofSize:17)
        buttonSend.setTitle(NSLocalizedString("FRQ_SEND_BUTTON", comment:""), for:.normal)

        let textFieldArray:[MDWTextFieldView] = [tfGivenName, tfFamilyName,tfCity, tfAge, tfEducation]
        
        for (index, item) in textFieldArray.enumerated(){
            
            item.fieldText.enablesReturnKeyAutomatically = false
            item.animationController = animationController
            animationController.textFields.append(item)
            
            item.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addSubview(item)
            
            NSLayoutConstraint.activate(    [
                (index == 0 ?
                    item.topAnchor.constraint(equalTo:textView.bottomAnchor):
                    item.topAnchor.constraint(equalTo:textFieldArray[index - 1].bottomAnchor, constant:20)
                )   ,
                item.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:20),
                item.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-20),
                item.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -40),
                item.heightAnchor.constraint(equalToConstant:60),
                ] )
        }
        
        buttonSend.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(buttonSend)
        
        buttonSend.heightAnchor.constraint(equalToConstant:60).isActive = true
        buttonSend.topAnchor.constraint(equalTo:tfEducation.bottomAnchor, constant:20).isActive = true
        buttonSend.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:20).isActive = true
        buttonSend.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -40).isActive = true
        buttonSend.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-20).isActive = true
        
        scrollView.bottomAnchor.constraint(equalTo: buttonSend.bottomAnchor, constant: 30).isActive = true
        
        animationController.returnController = MDWTextFieldReturnController(with:animationController, alwaysReturn:true)
        animationController.contentScrollView = self.scrollView
     
        if SessionManager.shared.sessionToken == nil {
            addAcceptLoginView()
        }
    }
    
    @objc func sendForm() -> Void {
        
        guard let gn = tfGivenName.fieldText.text, let fn = tfFamilyName.fieldText.text, let city = tfCity.fieldText.text, let age = Int(tfAge.fieldText.text ?? "0"), let edu = tfEducation.fieldText.text else {
            
            DispatchQueue.main.async {
                ErrorAlertNotificationView.showingAlert(in:self.view, info:"Enter all data in form" , type:.error)
            }
            return
        }
        let form = MDWFormiqueForm(givenName:gn, familyName:fn, city:city, age:age, category:"02", education:edu, images:[String](), email:nil, comment:nil)
        guard let token = SessionManager.shared.sessionToken else {return}
        
        NetworkManager.shared.postForm(form, token:token, completion:{ (aResponse, aErr) in
            guard let errorStr = aErr else {return}
            DispatchQueue.main.async {
                ErrorAlertNotificationView.showingAlert(in:self.view, info:errorStr , type:.error)
            }
        })
    }
    
    func addAcceptLoginView() -> Void {
        
        let vc = AcceptLoginViewController()
        addChild(vc)
        vc.didMove(toParent:self)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(vc.view)
        NSLayoutConstraint.activate([
            vc.view.rightAnchor.constraint(equalTo:view.rightAnchor),
            vc.view.leftAnchor.constraint(equalTo:view.leftAnchor),
            vc.view.bottomAnchor.constraint(equalTo:view.bottomAnchor),
            vc.view.topAnchor.constraint(equalTo:navBarView.bottomAnchor),
            ])
        
        vc.buttonHandler = {
            SessionManager.shared.sessionToken = nil
            SessionManager.shared.sessionStatus = .none
            self.manageCoordinator?.finishFlow?()
        }
    }
}

extension FormiqueVC:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView:UIScrollView) {
        navBarView.isSeparated = (scrollView.contentOffset.y > MDWConstant.OffsetForShowingSeparator)
    }
}
