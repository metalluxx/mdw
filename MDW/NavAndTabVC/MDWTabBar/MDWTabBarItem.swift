//
//  MDWTabBarItem.swift
//  MDW
//
//  Created by Metalluxx on 09/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit



public class MDWTabBarItem:UIView {
    weak var tabBarController:MDWTabBarController?
    
    var data:MDWTabBarItemField?{
        didSet{
            guard let data = data else { return }
            label.text = data.title
            imageView.image = data.image
        }
    }
    
    private var imageView = UIImageView()
    private var label = UILabel()
    private var button = UIButton()
    
    convenience init(item:MDWTabBarItemField,tabBarController:MDWTabBarController){
        self.init(frame:CGRect.zero)
        self.configure(item)
        self.tabBarController = tabBarController
    }
    
    override init(frame:CGRect) {
        super.init(frame:CGRect.zero)
        
        imageView.contentMode = .scaleAspectFit
        label.font = MDWConstant.normalFont(ofSize:13)
        label.textAlignment = .center
        label.numberOfLines = 1
        button.addTarget(self, action:#selector(tappedItem), for:.touchUpInside)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        button.translatesAutoresizingMaskIntoConstraints = false
        
        insertSubview(imageView, at: 0)
        insertSubview(label, at: 0)
        insertSubview(button, at: 0)
        
        NSLayoutConstraint.activate([
            label.heightAnchor.constraint(equalToConstant:15),
            label.leftAnchor.constraint(equalTo:leftAnchor),
            label.rightAnchor.constraint(equalTo:rightAnchor),
            label.bottomAnchor.constraint(equalTo:bottomAnchor, constant:-4),
            
            imageView.bottomAnchor.constraint(equalTo:label.topAnchor, constant:-7),
            imageView.leftAnchor.constraint(equalTo:leftAnchor),
            imageView.rightAnchor.constraint(equalTo:rightAnchor),
            imageView.topAnchor.constraint(equalTo:topAnchor, constant:8),
            
            button.topAnchor.constraint(equalTo:topAnchor),
            button.bottomAnchor.constraint(equalTo:bottomAnchor),
            button.leftAnchor.constraint(equalTo:leftAnchor),
            button.rightAnchor.constraint(equalTo:rightAnchor),
            ])
    }
    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
    }
    
    @objc func tappedItem() -> Void {
        guard let tabBarController = tabBarController else {
            print("Dont have tabBarController instance in \(self)")
            return
        }
        tabBarController.selectedItem(sender:self)
    }
    
    public func configure(_ field:MDWTabBarItemField) {
        data = field
    }
    
    public func setState(isSelected:Bool) {
        if isSelected {
            imageView.image = data?.image
        } else {
            imageView.image = data?.selectedImage
        }
    }
}




