//
//  MDWTabBarController.swift
//  MDW
//
//  Created by Metalluxx on 10/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class MDWTabBarController:CoordinatorViewController,MDWTabBarSelectProtocol{
    weak var manageCoordinator: BaseCoordinator?

    private var contentView = UIView()
    private var tabBar = MDWTabBar()

    
    var viewControllers = [MDWTabBarContentVC](){
        didSet{
            var arrayItems = [MDWTabBarItem]()
            for vc in viewControllers {
                let tbItem = MDWTabBarItem(item:vc.mdwTabBarItem, tabBarController:self)
                arrayItems.append(tbItem)
                
                guard let vc = vc as? CoordinatorViewController else {continue}
                vc.manageCoordinator = self.manageCoordinator
            }
            self.tabBar.tabBarItems = arrayItems
            currentViewController = viewControllers[0]
        }
    }
    
    func selectedItem(sender: MDWTabBarItem) {
        for vc in viewControllers {
            guard let item = sender.data else{continue}
            if vc.mdwTabBarItem == item{
                currentViewController = vc
            }
        }
    }
    
    var currentViewController:MDWTabBarContentVC? {
        willSet{switchViewController(from:currentViewController, to:newValue)}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        view.insertSubview(tabBar, at:0)
        view.insertSubview(contentView, at:0)
        
        NSLayoutConstraint.activate([
            tabBar.bottomAnchor.constraint(equalTo:view.bottomAnchor),
            tabBar.leftAnchor.constraint(equalTo:view.leftAnchor),
            tabBar.rightAnchor.constraint(equalTo:view.rightAnchor),
            tabBar.topAnchor.constraint(equalTo: tabBar.safeAreaLayoutGuide.bottomAnchor, constant: -60),
            
            contentView.topAnchor.constraint(equalTo:view.topAnchor),
            contentView.leftAnchor.constraint(equalTo:view.leftAnchor),
            contentView.rightAnchor.constraint(equalTo:view.rightAnchor),
            contentView.bottomAnchor.constraint(equalTo:tabBar.topAnchor),
            ])
    }
    
    func switchViewController(from:UIViewController?, to:UIViewController?) -> Void {
        if let from = from, let to = to {
            UIView.transition(from: from.view, to: to.view, duration: 0.1, options: .transitionCrossDissolve)
        }
        
        if from != nil{
            from!.willMove(toParent:nil)
            from!.view.removeFromSuperview()
            from!.removeFromParent()
        }
        if to != nil {
            addChild(to!)
            to!.view.translatesAutoresizingMaskIntoConstraints = false
            contentView.insertSubview(to!.view, at:0)
            NSLayoutConstraint.activate([
                to!.view.topAnchor.constraint(equalTo: self.contentView.topAnchor),
                to!.view.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
                to!.view.leftAnchor.constraint(equalTo: self.contentView.leftAnchor),
                to!.view.rightAnchor.constraint(equalTo: self.contentView.rightAnchor),
                ])
            to!.didMove(toParent:self)
        }
        

    }
}
