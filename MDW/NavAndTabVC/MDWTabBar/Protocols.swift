//
//  Protocols .swift
//  MDW
//
//  Created by Metalluxx on 11/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public protocol MDWTabBarItemFieldEntity {
    var image:UIImage? {get set}
    var selectedImage:UIImage? {get set}
    var title:String {get set}
}

public struct MDWTabBarItemField:MDWTabBarItemFieldEntity {
    public var image:UIImage?
    public var selectedImage:UIImage?
    public var title:String
    public var id:UInt32 = 0
    init(image:UIImage?,selectedImage:UIImage?,title:String) {
        self.image = image
        self.selectedImage = selectedImage
        self.title = title
        self.id = arc4random()
    }
    static func == (left: MDWTabBarItemField, right: MDWTabBarItemField) -> Bool {
        return left.id == right.id
    }
}

typealias MDWTabBarContentVC = UIViewController & MDWTabBarItemContainer

public protocol MDWTabBarSelectProtocol:class{
    func selectedItem(sender:MDWTabBarItem) -> Void
}

protocol MDWTabBarItemContainer where Self:UIViewController {
    var mdwTabBarItem:MDWTabBarItemField {get set}
}



