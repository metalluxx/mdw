//
//  MDWTabControllerViewController.swift
//  MDW
//
//  Created by Metalluxx on 17/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class MDWTabController:UITabBarController,CoordinatorManager  {
    weak var manageCoordinator: BaseCoordinator?{
        didSet{
            guard let viewControllers = viewControllers else {return}
            for vc in viewControllers {
                guard let vc = vc as? CoordinatorViewController else {continue}
                vc.manageCoordinator = self.manageCoordinator
            }
        }
    }
    
    static var itemOffset = UIOffset(horizontal:0, vertical:-4)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vcs = [ ProjectVC(), FormiqueVC(), MapVC(), InfoVC() ]
        setViewControllers(vcs , animated:true)
        
        selectedIndex = 0
        
        tabBar.tintColor = UIColor(red:0, green:0, blue:0, alpha:1)
        tabBar.unselectedItemTintColor = UIColor(red:0.6, green:0.6, blue:0.6, alpha:1)
        tabBar.barTintColor = .white
        tabBar.isTranslucent = false
        tabBar.layer.borderColor = UIColor.red.cgColor
        UITabBarItem.appearance().setTitleTextAttributes([.font:MDWConstant.normalFont(ofSize: 13)], for:.normal)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let newTabBarHeight = tabBar.frame.size.height + 5.0
        var newFrame = tabBar.frame
        newFrame.size.height = newTabBarHeight
        newFrame.origin.y = view.frame.size.height - newTabBarHeight
        tabBar.frame = newFrame
    }

    
}

