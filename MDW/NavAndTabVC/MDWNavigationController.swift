//
//  MDWNavController.swift
//  MDW
//
//  Created by Metalluxx on 17/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class MDWNavigationController:UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        isNavigationBarHidden = true
        view.backgroundColor = .gray
    }
}


