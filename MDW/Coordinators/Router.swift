//
//  Router.swift
//  MDW
//
//  Created by Metalluxx on 15/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class Router:Routable {
    private(set) static var shared = Router()
    
    var navigationController = MDWNavigationController()
    var window:UIWindow!
    
    init() {
        window = UIWindow(frame:UIScreen.main.bounds)
        window.backgroundColor = UIColor.white
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    func setRootModule(_ module:Presentable?) {
        guard let toVC = module?.currentPresentor else {return}
        guard let fromVC = navigationController.viewControllers.last else {
            navigationController.setViewControllers([toVC], animated: false)
            return
        }
        self.navigationController.setViewControllers([toVC], animated: false)
        UIWindow.transition(from: fromVC.view, to: toVC.view, duration: 0.6, options: [.transitionFlipFromLeft, .curveEaseInOut])
    }
    
    func setRootModule(_ module:Presentable?, transitionOption:UIView.AnimationOptions) {
        guard let toVC = module?.currentPresentor else {return}
        guard let fromVC = navigationController.viewControllers.last else {
            navigationController.setViewControllers([toVC], animated: false)
            return
        }
        self.navigationController.setViewControllers([toVC], animated: false)
        UIWindow.transition(from: fromVC.view, to: toVC.view, duration: 0.6, options: transitionOption)
    }
    
    func push(_ module: Presentable?) {
        push(module, animated: true, completion: nil)
    }
    
    func push(_ module: Presentable?, animated: Bool) {
        push(module, animated: animated, completion: nil)
    }
    
    func push(_ module: Presentable?, animated: Bool, completion: VoidBlock?) {
        guard let vc = module?.currentPresentor else {return}
        navigationController.pushViewController(vc, animated: animated)
        completion?()
    }
    
    func popModule() {
        popModule(animated: true)
    }
    
    func popModule(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }
    
//    func dismissModule() {
//        dismissModule(animated: true, completion: nil)
//    }
//
//    func dismissModule(animated: Bool, completion: CompletionBlock?) {
//        window.rootViewController?.dismiss(animated: animated, completion: completion)
//    }
//
//    func present(_ module: Presentable?) {
//        present(module, animated: true)
//    }
//
//    func present(_ module: Presentable?, animated: Bool) {
//        guard let vc = module?.currentPresentor else {return}
//        window.rootViewController?.present(vc, animated: animated, completion: {
//
//        })
//    }
}
