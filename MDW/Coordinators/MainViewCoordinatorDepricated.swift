//
//  MainViewCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 16/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class MainViewCoordinatorDepricated:BaseCoordinator,Presentable {
    weak var currentPresentor:CoordinatorViewController?
    deinit {
        print("Dealloc \(self)")
    }
    override func start() {
        DispatchQueue.main.async {
            let vc = MDWTabController()
            self.currentPresentor = vc
            self.currentPresentor?.manageCoordinator = self
            self.router.setRootModule(self,transitionOption:[.transitionFlipFromRight, .curveEaseInOut])
        }
    }
}
