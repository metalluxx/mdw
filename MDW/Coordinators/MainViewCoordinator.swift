//
//  MDWTabControllerCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 18/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit


class MainViewCoordinator:BaseCoordinator,Presentable{
    weak var currentPresentor: CoordinatorViewController?
    deinit {
        print("Dealloc \(self)")
    }
    override func start() {
        let vc = MDWTabBarController()
        currentPresentor = vc
        currentPresentor?.manageCoordinator = self
        vc.viewControllers = [ProjectVC(),FormiqueVC(),MapVC(),InfoVC()]
        router.setRootModule(self)
    }
}
