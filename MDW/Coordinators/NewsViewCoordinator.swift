//
//  NewsViewCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 17/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class NewsViewCoordinator:BaseCoordinator,Presentable{
    weak var currentPresentor: CoordinatorViewController?
    var news:MDWMaterial?
    
    deinit {
        print("dealloc \(self)")
    }
    convenience init(router:Routable,news:MDWMaterial) {
        self.init(router:router)
        self.news = news
    }
    override func start() {
        guard let news = news else {
            finishFlow?()
            return
        }
        let vc = NewsViewController(material:news)
        self.currentPresentor = vc
        self.currentPresentor?.manageCoordinator = self
        router.push(self, animated: true)
    }
}
