//
//  BaseCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 14/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class BaseCoordinator:Coordinatable{
    func start() {
        fatalError("This method is dont overrided")
    }
    var finishFlow:VoidBlock?
//    {
//        get{
//            guard let finishFlow = self.finishFlow else {
//                guard let patentCoordinator = self.parentCoordinator else {return nil}
//                return patentCoordinator.finishFlow
//            }
//            return finishFlow
//        }
//    }
    private(set) var router:Routable
    
    var childCoordinators: [Coordinatable] = []
    private(set) weak var parentCoordinator:BaseCoordinator?
    
    init(router:Routable) {
        self.router = router
    }
    
    convenience init(router:Routable, parent:BaseCoordinator?) {
        self.init(router:router)
        parentCoordinator = parent
    }

    func addDependency(_ coordinator: Coordinatable) {
        for element in childCoordinators {
            if element === coordinator { return }
        }
        childCoordinators.append(coordinator)
    }
    
    func removeDependency(_ coordinator: Coordinatable?) {
        guard
            childCoordinators.isEmpty == false,
            let coordinator = coordinator
            else { return }
        
        for (index, element) in childCoordinators.enumerated() {
            if element === coordinator {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
}
