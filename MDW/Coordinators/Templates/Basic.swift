//
//  Basic.swift
//  MDW
//
//  Created by Metalluxx on 14/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

typealias VoidBlock = () -> ()
typealias CoordinatorViewController = UIViewController & CoordinatorManager

protocol Coordinatable:class{
    func start()
    var finishFlow:VoidBlock? {get set}
}
protocol Presentable {
    /// Weak reference to view for present from coordinator
    var currentPresentor:CoordinatorViewController? { get set }
}

