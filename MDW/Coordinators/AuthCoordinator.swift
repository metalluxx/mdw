//
//  AuthCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 15/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class AuthCoordinator:BaseCoordinator,Presentable{
    weak var currentPresentor: CoordinatorViewController?
    deinit {
        print("Dealloc \(self)")
    }
    override func start() {
        DispatchQueue.main.async {
            let vc = LoginViewController()
            self.currentPresentor = vc
            self.currentPresentor?.manageCoordinator = self
            self.router.setRootModule(self,transitionOption:[.transitionFlipFromLeft, .curveEaseInOut])
        }
    }
}

