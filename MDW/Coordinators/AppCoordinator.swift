//
//  AppCoordinator.swift
//  MDW
//
//  Created by Metalluxx on 15/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class AppCoordinator:BaseCoordinator{
    override func start() {
        switch SessionManager.shared.sessionStatus {
        case .none:
            showLoginFlow()
        default:
            showMainFlow()
        }
    }
    func showLoginFlow() {
        CoordinatorFactory.createLoginCoordinator(parentCoordinator:self).start()
    }
    func showMainFlow() {
        CoordinatorFactory.createMainCoordinator(parentCoordinator:self).start()
    }
}
