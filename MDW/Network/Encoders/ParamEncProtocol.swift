//
//  ParametersEncoderProtocol.swift
//  MDW
//
//  Created by Metalluxx on 02/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation


protocol ParameterEncoder {
    static func encode(urlRequest:inout URLRequest, with parameters:Parameters) throws
}
