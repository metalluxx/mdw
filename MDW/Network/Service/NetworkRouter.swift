//
//  NetworkRouter.swift
//  MDW
//
//  Created by Metalluxx on 02/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public typealias NetworkRouterCompletion = (_ data:Data?,_ response:URLResponse?, _ error:Error?) -> ()
public typealias NetworkRouterDownlaodCompletion = (_ url:URL?,_ response:URLResponse?, _ error:Error?) -> ()

protocol NetworkRouter:class {
    associatedtype EndPoint:EndPointTypeTemplate
    @discardableResult func dataRequest(_ route:EndPoint, completion:@escaping NetworkRouterCompletion) -> URLSessionDataTask?
    @discardableResult func downloadRequest(_ route:EndPoint, completion:@escaping NetworkRouterDownlaodCompletion) -> URLSessionDownloadTask?
    func cancel(task: URLSessionTask)
}

enum NetworkRouterError:String,Error {
    case dontDownloadTask
}

class NetRouter<EndPoint:EndPointTypeTemplate>:NetworkRouter {
    var tasks = [URLSessionTask]()
    deinit {
        for item in tasks {
            item.cancel()
        }
    }
    
    //MARK: - Create requests
    @discardableResult func downloadRequest(_ route:EndPoint, completion:@escaping NetworkRouterDownlaodCompletion) -> URLSessionDownloadTask? {
        var task:URLSessionDownloadTask?
        if route.task.isDownloadTask(){
            task = URLSession.shared.downloadTask(with: route.baseURL, completionHandler: { (url, response, error) in
                self.remove(task: task)
                completion(url, response, error)
            })
        }
        else {
            completion(nil, nil, NetworkRouterError.dontDownloadTask)
        }
        
        guard task != nil else { return nil }
        tasks.append(task!)
        task!.resume()
        return task
    }
    
    @discardableResult func dataRequest(_ route:EndPoint, completion:@escaping NetworkRouterCompletion) -> URLSessionDataTask? {
        var task:URLSessionDataTask?
        do {
            let request = try self.buildRequest(from:route)
            task = URLSession.shared.dataTask(with:request, completionHandler:{ (data, response, error) in
                self.remove(task: task)
                completion(data,response, error)
            })
        }
        catch{ completion(nil, nil, error) }
        
        guard task != nil else { return nil }
        tasks.append(task!)
        task!.resume()
        return task
    }
    
    //MARK: - Manage tasks
    @discardableResult private func remove(task: URLSessionTask?) -> Bool {
        guard let task = task else { return false }
        
        for (index,item) in tasks.enumerated(){
            if item == task{
                tasks.remove(at: index)
                return true
            }
        }
        return false
    }
    
    
    func cancel(task: URLSessionTask) -> Void {
        remove(task: task) ? task.cancel() : nil
    }
    
    //MARK: - Manage requests
    func buildRequest(from route:EndPoint) throws -> URLRequest {
        var request = URLRequest(url:route.baseURL.appendingPathComponent(route.path), cachePolicy:.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval:10.0)
        
        request.httpMethod = route.httpMethod.rawValue
        
        do {
            switch route.task {
            case .requestDownloadFile: break
            case .request:
                request.addValue("application/json", forHTTPHeaderField:"Content-Type")
            case .requestParameters(let bodyParameters, let urlParameters):
                try self.configureParameters(bodyParameters:bodyParameters, urlParameters:urlParameters, request:&request )
            case .requestParametersAndHeaders(let bodyParameters, let urlParameters, let additionalHeaders):
                self.addAdditionalHeaders(additionalHeaders, request:&request)
                try self.configureParameters(bodyParameters:bodyParameters, urlParameters:urlParameters, request:&request )
            }
            return request
            
        } catch  {
            throw error
        }
    }
    
    func configureParameters(bodyParameters:Parameters?, urlParameters:Parameters?, request:inout URLRequest ) throws {
        do {
            if let bodyParameters = bodyParameters {
                try JSONParameterEncoder.encode(urlRequest:&request , with:bodyParameters)
            }
            if let urlParameters = urlParameters {
                try URLParameterEncoder.encode(urlRequest:&request, with:urlParameters)
            }
        } catch  { throw error }
    }
    
    func addAdditionalHeaders(_ additionalHeaders:HTTPHeaders?, request:inout URLRequest)  {
        guard let additionalHeaders = additionalHeaders else { return }
        for (key, value) in additionalHeaders{
            request.setValue(value, forHTTPHeaderField:key)
        }
    }
}
