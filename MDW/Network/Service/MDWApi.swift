//
//  MDWApi.swift
//  MDW
//
//  Created by Metalluxx on 02/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public enum MDWApi {
    case getAuth(phone:String)
    case getToken(key:MDWAuthKey, code:String)
    case downloadsShortMaterials
    case getFullMaterial(shortMaterial:MDWShortMaterial)
    case sendForm(token:MDWAuthToken, form:MDWFormiqueForm)
    case downloadImage(url:URL)
}

extension MDWApi:EndPointTypeTemplate{
    var headers:HTTPHeaders? {return nil}
    var baseURL:URL {
        switch self {
        case .downloadImage(let url):return url
        default:return MDWNetwork.apiUrl!
        }
    }
    var path:String {
        switch self {
        case .getAuth(_):   return "auth"
        case .getToken(_):  return "verify"
        case .downloadsShortMaterials:  return "special"
        case .getFullMaterial(let shortMaterial):   return "special/\(shortMaterial.id)"
        case .sendForm(_, _): return "competition/5b80849e30a1e21c31c7d7f2/"
        case .downloadImage(_): return ""
        }
    }
    var task:HTTPTask {
        switch self {
        case .getAuth(phone:let phone):
            return .requestParameters(bodyParameters:["phone":phone], urlParameters:nil)
        case .getToken(key:let key, code:let code):
            return .requestParameters(bodyParameters:["key":key.key ?? "" , "code":code], urlParameters:nil)
        case .downloadsShortMaterials:
            return .requestParameters(bodyParameters:nil, urlParameters:["year":"2018"])
        case .getFullMaterial(let shortMaterial):
            return.requestParameters(bodyParameters:nil, urlParameters:[shortMaterial.id:""])
        case .sendForm(let token, _):
            guard let tokenString = token.token else { return .requestParameters(bodyParameters:nil, urlParameters:nil) }
            return .requestParametersAndHeaders(bodyParameters:nil, urlParameters:["5b80849e30a1e21c31c7d7f2":""], additionHeaders:["token":tokenString])
        case .downloadImage(_):
            return .requestDownloadFile
        }
    }
    var httpMethod:HTTPMethod {
        switch self {
        case .getAuth(_):return .post
        case .getToken(_):return .post
        case .downloadsShortMaterials:return .get
        case .getFullMaterial(_):return .get
        case .sendForm(_,_):return .post
        case .downloadImage(_): return .none
        }
    }
}
