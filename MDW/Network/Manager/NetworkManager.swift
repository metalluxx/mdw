//
//  NetworkManager.swift
//  MDW
//
//  Created by Metalluxx on 02/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import UIKit


enum NetworkResponse:String {
    case success
    case dontConnectionInternet = "Please check your network connection"
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
    case incorrectUrl = "Unable to convert string to link"
}


enum Result<String> {
    case success
    case failure(String)
}

struct NetworkManager {
    static let shared = NetworkManager()
    
    let router = NetRouter<MDWApi>()
    
    func getKey(phone:String, completion:@escaping (_ key:MDWAuthKey?, _ error:String?) -> () )  {
        router.dataRequest(MDWApi.getAuth(phone:phone)) { (data, response, error) in
            if error != nil {
                completion(nil, error!.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            let result = self.handleNetworkResponse(response)
                
            switch result{
            case .success:
                guard let responseData = data else {
                    completion(nil, NetworkResponse.noData.rawValue)
                    return
                }
                do{
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let apiResponse = try decoder.decode(MDWAuthKey.self, from:responseData)
                    completion(apiResponse, nil)
                    return
                }
                catch{
                    completion(nil, NetworkResponse.unableToDecode.rawValue)
                    return
                }
            case .failure(let networkFailureError):
                completion(nil, networkFailureError)
                return
            }
        }   
    }
    
    func getToken(key:MDWAuthKey, code:String, completion:@escaping (_ key:MDWAuthToken?, _ error:String?) -> () )  {
        router.dataRequest(MDWApi.getToken(key:key, code:code)) { (data, response, error) in
            if error != nil {
                completion(nil, error!.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            let result = self.handleNetworkResponse(response)
            
            switch result{
            case .success:
                guard let responseData = data else {
                    completion(nil, NetworkResponse.noData.rawValue)
                    return
                }
                do{
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let apiResponse = try decoder.decode(MDWAuthToken.self, from:responseData)
                    completion(apiResponse, nil)
                }
                catch{
                    completion(nil, NetworkResponse.unableToDecode.rawValue)
                }
                
            case .failure(let networkFailureError):
                completion(nil, networkFailureError)
            }
        }
    }
    
    func downloadShortMaterials( completion:@escaping (_ key:[MDWShortMaterial]?, _ error:String?) -> () )  {
        router.dataRequest(MDWApi.downloadsShortMaterials) { (data, response, error) in
            if error != nil {
                completion(nil, error!.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            let result = self.handleNetworkResponse(response)
                
            switch result{
            case .success:
                guard let responseData = data else {
                    completion(nil, NetworkResponse.noData.rawValue)
                    return
                }
                do{
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let apiResponse = try decoder.decode([MDWShortMaterial].self, from:responseData)
                    completion(apiResponse, nil)
                }
                catch{
                    completion(nil, NetworkResponse.unableToDecode.rawValue)
                }
            case .failure(let networkFailureError):
                completion(nil, networkFailureError)
            }
        }
    }
    
    func downloadImage(urlPath:String, completion:@escaping(_ image:UIImage?, _ error:String?) -> () )  {
        guard let url = URL(string: urlPath) else {
            completion(nil, NetworkResponse.incorrectUrl.rawValue)
            return
        }
        router.downloadRequest(MDWApi.downloadImage(url: url)) { (data, response, error) in
            if error != nil {
                completion(nil, error!.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            let result = self.handleNetworkResponse(response)
            switch result{
            case .success:
                guard let safeUrl = data else {
                    completion(nil, NetworkResponse.noData.rawValue)
                    return
                }
                do{
                    let image = UIImage(data: try Data(contentsOf: safeUrl))
                    completion(image, nil)
                }
                catch{
                    completion(nil, NetworkResponse.unableToDecode.rawValue)
                }
            case .failure(let networkFailureError):
                completion(nil, networkFailureError)
            }
        }
    }
    
    func downloadFullMaterial( shortMaterial:MDWShortMaterial,  completion:@escaping (_ fullMaterial:MDWMaterial?, _ error:String?) -> () )  {
        router.dataRequest(MDWApi.getFullMaterial(shortMaterial:shortMaterial)) { (data, response, error) in
            if error != nil {
                completion(nil, error!.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            let result = self.handleNetworkResponse(response)
        
            switch result{
            case .success:
                guard let responseData = data else {
                    completion(nil, NetworkResponse.noData.rawValue)
                    return
                }
                do{
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let apiResponse = try decoder.decode(MDWMaterial.self, from:responseData)
                    completion(apiResponse, nil)
                }
                catch {
                    completion(nil, NetworkResponse.unableToDecode.rawValue)
                }
            case .failure(let networkFailureError):
                completion(nil, networkFailureError)
            }
        }
    }
    
    
    func postForm(_ form:MDWFormiqueForm, token:MDWAuthToken  , completion:@escaping (_ response:URLResponse? , _ error:String?) -> () )  {
        router.dataRequest(MDWApi.sendForm(token:token, form:form)) { (data, response, error) in
            if error != nil {
                completion(response ,error!.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else { return }
            let result = self.handleNetworkResponse(response)
            switch result{
            case .success:  completion(response, nil)
            case .failure(let networkFailureError):  completion(nil, networkFailureError)
            }
        }
    }
    
    
    fileprivate func handleNetworkResponse(_ response:HTTPURLResponse) -> Result<String>{
        switch response.statusCode {
        case 200...299:return .success
        case 401...500:return .failure(NetworkResponse.authenticationError.rawValue)
        case 501...599:return .failure(NetworkResponse.badRequest.rawValue)
        case 600:return .failure(NetworkResponse.outdated.rawValue)
        default:return .failure(NetworkResponse.failed.rawValue)
        }
    }
    
}



