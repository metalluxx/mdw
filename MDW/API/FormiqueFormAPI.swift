//
//  FormiqueForm.swift
//  MDW
//
//  Created by Metalluxx on 09/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public struct MDWFormiqueForm:Codable {
    var givenName:String
    var familyName:String
    var city:String
    var age:Int
    var category = "02"
    var education:String
    var images = [String]()
    var email:String?
    var comment:String?
    }

