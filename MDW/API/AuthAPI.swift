//
//  AuthAPI.swift
//  MDW
//
//  Created by Metalluxx on 30/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

public struct MDWAuthKey:Codable {
    var key:String?
    var error:MDWErrorResponse?
}

public struct MDWAuthToken:Codable {
    var success:Bool
    var token:String?
    var user:MDWUser?
}
