//
//  ApiStructes.swift
//  MDW
//
//  Created by Metalluxx on 22/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

public struct MDWImage:Codable {
    var width:Int
    var height:Int
    var preview:String
    var original:String
    var getOriginalURL:URL? {
        return URL(string:original)
    }
    var getPreviewImage:UIImage? {
        let editedPreview = preview.replacingOccurrences(of:"data:image/jpeg;base64,", with:"")
        guard let data = Data(base64Encoded:editedPreview) else {
            return nil
        }
        return UIImage(data:data) ?? nil
    }
}




