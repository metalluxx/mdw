//
//  MaterialAPI.swift
//  MDW
//
//  Created by Metalluxx on 30/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

protocol MDWEntity {
    var id:String {get set}
    var permalink:String  {get set}
}

public enum MDWMaterialType:String, Codable {
    case article, gallery, project, competition, event, participant
}

public struct MDWShortMaterial:Codable, MDWEntity {
    var id:String
    var permalink:String
    
    var title:String?
    var description:String?
    var cover:MDWImage
    var type:MDWMaterialType
    var year:Int
    var published:String?
}

public struct MDWMaterial:MDWEntity, Codable {
    var id:String
    var permalink:String
    
    var title:String?
    var description:String?
    var fullCover:MDWImage
    var blocks:[MDWMaterialBlock]
    var type:MDWMaterialType
    var year:Int
    var published:String?
}

