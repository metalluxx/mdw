//
//  UserAPI.swift
//  MDW
//
//  Created by Metalluxx on 30/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation

protocol MDWUserProtocol {
    var id:String {get set}
    var givenName:String {get set}
    var familyName:String {get set}
    var phone:String {get set}
    var email:String {get set}
}
public struct MDWShortUser:MDWUserProtocol, Codable {
    var id:String
    var givenName:String
    var familyName:String
    var phone:String
    var email:String
}

public struct MDWUser:MDWUserProtocol, Codable {
    var id:String
    var givenName:String
    var familyName:String
    var phone:String
    var email:String
    
    var city:String?
    var age:Int?
    var education:String?
    var company:String?
    var role:String?
}

