//
//  MDWMaterialBlock.swift
//  MDW
//
//  Created by Metalluxx on 24/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

enum MDWMaterialBlockType:String , Codable{
    case paragraph, image
}

public struct MDWMaterialBlock:Codable {
    var type:MDWMaterialBlockType
    var content:MDWMaterialBlockContent
}

public struct MDWMaterialBlockContent:Codable {
    var title:String?
    var text:String?
    var image:MDWImage?
}
