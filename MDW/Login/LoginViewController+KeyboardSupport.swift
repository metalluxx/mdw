//
//  LoginViewController+KeyboardSupport.swift
//  MDW
//
//  Created by Metalluxx on 17/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import UIKit

extension LoginViewController {
    
    func enableObserverForKeyboard() -> Void {
        NotificationCenter.default.addObserver(self, selector:#selector(showedKeyboard(notification:)), name:UIResponder.keyboardWillShowNotification,  object:nil)
        NotificationCenter.default.addObserver(self, selector:#selector(hiddenKeyboard), name:UIResponder.keyboardWillHideNotification , object:nil)
    }
    
    @objc func showedKeyboard(notification:Notification) -> Void {
        scrollView.contentOffset = CGPoint(x:0.0 , y:(notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height - 60 - UIApplication.shared.keyWindow!.safeAreaInsets.bottom)
    }
    
    @objc func hiddenKeyboard() -> Void {
        scrollView.contentOffset = .zero
    }
}
