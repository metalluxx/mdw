//
//  LoginViewController+TargetAction.swift
//  MDW
//
//  Created by Metalluxx on 17/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

extension LoginViewController {

    @objc func enterTappedForSendSMS(sender:UIButton) -> Void {
        isSendedSMS = true
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        NetworkManager.shared.getKey(phone:numberTextField.fieldText.text ?? ""  , completion:{ (key, err) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                guard let unswapedKey = key else {
                    self.isSendedSMS = false
                    self.numberTextField.lineColor = MDWConstant.TextFieldColors.errorLine
                    self.errorView?.removeFromSuperview()
                    self.errorView = ErrorAlertNotificationView.showingAlert(in: self.view, info:err ?? NSLocalizedString("ERROR", comment:""), type:MDWAlertNotificationType.error)
                    return
                }
                
                self.isSendedSMS = true
                self.numberTextField.lineColor = MDWConstant.TextFieldColors.defaultLine
                self.key = unswapedKey
                self.codeTextField.fieldText.isEnabled = true
                self.codeTextField.fieldText.becomeFirstResponder()
                self.loginButton.setTitle(NSLocalizedString("LVC_SEND_PHONE", comment:""), for:.normal)
                self.loginButton.removeTarget(self, action:#selector(self.enterTappedForSendSMS), for:.touchUpInside)
                self.loginButton.addTarget(self, action:#selector(self.enterTappedForRunVC), for:.touchUpInside)
            }
        })
    }
    
    @objc func enterTappedForRunVC(sender:UIButton) -> Void {
        isCheckCode = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        guard let unswapKey = self.key else {
            DispatchQueue.main.async {
                self.isCheckCode = false
                self.codeTextField.fieldText.resignFirstResponder()
                self.codeTextField.lineColor = MDWConstant.TextFieldColors.errorLine
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            self.errorView?.removeFromSuperview()
            self.errorView = ErrorAlertNotificationView.showingAlert(in:self.view, info:"Unable swapping key", type:.error)
            return
        }
        
        NetworkManager.shared.getToken(key:unswapKey, code:codeTextField.fieldText.text ?? "" , completion:{ (verify, err) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            guard let unswapVerify = verify else {
                DispatchQueue.main.async {
                    self.isCheckCode = false
                    self.codeTextField.fieldText.resignFirstResponder()
                    self.codeTextField.lineColor = MDWConstant.TextFieldColors.errorLine
                }
    
                self.errorView?.removeFromSuperview()
                self.errorView = ErrorAlertNotificationView.showingAlert(in:self.view, info:err ?? NSLocalizedString("ERROR", comment:""), type:.error)
                return
            }
            
            DispatchQueue.main.async {
                self.codeTextField.lineColor = MDWConstant.TextFieldColors.defaultLine
                SessionManager.shared.sessionToken = unswapVerify
            }
            SessionManager.shared.sessionStatus = .user
            self.manageCoordinator?.finishFlow?()
        })
}
    
    @objc func enterObserveButton() -> Void {
        SessionManager.shared.sessionStatus = .guest
        self.manageCoordinator?.finishFlow?()
    }
}
