//
//  ViewController.swift
//  MDW
//
//  Created by Metalluxx on 14/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit


class LoginViewController:UIViewController, CoordinatorManager {
    weak var manageCoordinator: BaseCoordinator?
    
    var key:MDWAuthKey?
    var animationController:MDWTextFieldAnimationController!
    
    var numberTextField = MDWTextFieldView()
    var codeTextField = MDWTextFieldView()
    var errorView:ErrorAlertNotificationView?
    var scrollView = UIScrollView()
    var loginButton = UIButton()
    var observeButton = UIButton()
    
    var imageView = UIImageView(image:UIImage(named:"loginImage"))
    
    var isSendedSMS:Bool = false {
        didSet{
            if isSendedSMS {
                view.endEditing(true)
                numberTextField.fieldText.textColor = MDWConstant.LVCTextFieldColors.unfocus
                numberTextField.fieldText.isEnabled = false
            }
            else{
                numberTextField.fieldText.isEnabled = true
                numberTextField.fieldText.textColor = MDWConstant.LVCTextFieldColors.focus
            }
        }
    }
    
    var isCheckCode:Bool = false {
        didSet{
            if isCheckCode {
                view.endEditing(true)
                codeTextField.fieldText.textColor = MDWConstant.LVCTextFieldColors.unfocus
                codeTextField.fieldText.isEnabled = false
            }
            else{
                codeTextField.fieldText.isEnabled = true
                codeTextField.fieldText.textColor = MDWConstant.LVCTextFieldColors.focus
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animationController = MDWTextFieldAnimationController()
        enableObserverForKeyboard()
        setupInitialConstraints()
        setupUI()
    }

    func setupUI() -> Void {

        view.backgroundColor = .white
        
        scrollView.alwaysBounceVertical = false
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target:view, action:#selector(UIView.endEditing)))
        
        observeButton.backgroundColor = .clear
        observeButton.setTitle(NSLocalizedString("LVC_SKIP", comment:""), for:.normal)
        observeButton.titleLabel?.textAlignment = .center
        observeButton.setTitleColor(.black, for:.normal)
        observeButton.addTarget(self, action:#selector(self.enterObserveButton), for:.touchUpInside)
        observeButton.titleLabel?.font = MDWConstant.normalFont(ofSize:17)
        
        loginButton.backgroundColor = .black
        loginButton.titleLabel?.font = MDWConstant.normalFont(ofSize:17)
        loginButton.setTitle(NSLocalizedString("LVC_SEND_CODE", comment:""), for:.normal)
        loginButton.addTarget(self, action:#selector(self.enterTappedForSendSMS) , for:.touchUpInside)
        
        codeTextField.labelText.text = NSLocalizedString("LVC_CODE", comment:"")
        codeTextField.fieldText.tag = Int.random(in:Int.min...Int.max)
        codeTextField.fieldText.keyboardType = .numberPad
        codeTextField.fieldText.isEnabled = false
        animationController.textFields.append(codeTextField)
        codeTextField.fieldText.delegate = animationController
        
        numberTextField.labelText.text = NSLocalizedString("LVC_PHONE", comment:"")
        numberTextField.fieldText.tag = Int.random(in:Int.min...Int.max)
        numberTextField.fieldText.keyboardType = .phonePad
        animationController.textFields.append(numberTextField)
        numberTextField.fieldText.delegate = animationController
        
        imageView.contentMode = .scaleToFill
    }
    
    func setupInitialConstraints() {

        scrollView.translatesAutoresizingMaskIntoConstraints = false
        observeButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        codeTextField.translatesAutoresizingMaskIntoConstraints = false
        numberTextField.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(scrollView)
        
        scrollView.insertSubview(imageView, at: 0)
        scrollView.insertSubview(numberTextField, at: 0)
        scrollView.insertSubview(codeTextField, at: 0)
        scrollView.insertSubview(loginButton, at: 0)
        scrollView.insertSubview(observeButton, at: 0)
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo:view.layoutMarginsGuide.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo:view.layoutMarginsGuide.bottomAnchor),
            scrollView.leftAnchor.constraint(equalTo:view.leftAnchor),
            scrollView.rightAnchor.constraint(equalTo:view.rightAnchor),
            
            observeButton.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 280),
            observeButton.bottomAnchor.constraint(equalTo:scrollView.bottomAnchor, constant:-30),
            observeButton.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:20),
            observeButton.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-20),
            observeButton.heightAnchor.constraint(equalToConstant:20.0),

            loginButton.heightAnchor.constraint(equalToConstant:60),
            loginButton.bottomAnchor.constraint(equalTo:observeButton.topAnchor, constant:-20),
            loginButton.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:20),
            loginButton.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-20),

            codeTextField.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:20),
            codeTextField.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-20),
            codeTextField.bottomAnchor.constraint(equalTo:loginButton.topAnchor, constant:-46),
            codeTextField.heightAnchor.constraint(equalToConstant:60),

            numberTextField.heightAnchor.constraint(equalToConstant:60),
            numberTextField.rightAnchor.constraint(equalTo:scrollView.rightAnchor, constant:-20),
            numberTextField.leftAnchor.constraint(equalTo:scrollView.leftAnchor, constant:20),
            numberTextField.bottomAnchor.constraint(equalTo:codeTextField.topAnchor, constant:-20),
            
            imageView.widthAnchor.constraint(equalTo:scrollView.widthAnchor),
            imageView.leftAnchor.constraint(equalTo:scrollView.leftAnchor),
            imageView.rightAnchor.constraint(equalTo:scrollView.rightAnchor),
            imageView.topAnchor.constraint(equalTo:scrollView.topAnchor),
            imageView.heightAnchor.constraint(equalTo: scrollView.heightAnchor, constant: -336),
            ])
    }
}




