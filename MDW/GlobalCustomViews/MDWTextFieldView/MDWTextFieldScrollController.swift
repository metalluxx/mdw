
//
//  MDWTextFieldScrollController.swift
//  MDW
//
//  Created by Metalluxx on 08/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit



class MDWTextFieldScrollController:MDWTextFieldAnimationController {
    
    weak var contentScrollView:UIScrollView?
    private(set) weak var currentTextField:MDWTextFieldView?
    
    private var rectKeyboard = CGRect.zero
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector:#selector(insertRectangleKeyboardFromNotification(notification:)) , name:UIResponder.keyboardWillShowNotification, object:nil)
    }
    
    @objc private func insertRectangleKeyboardFromNotification(notification:Notification) -> Void {
        rectKeyboard = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
//        refreshOffset()
    }
    
    private func refreshOffset() -> Void {
        guard let currentTextField = currentTextField,  let scrollView = contentScrollView else { return }
        
        (rectKeyboard.origin.y < (currentTextField.frame.origin.y + currentTextField.frame.height + 100)) ?
            scrollView.contentOffset = CGPoint(x:0, y:(currentTextField.frame.origin.y + currentTextField.frame.height - rectKeyboard.origin.y + 100)) :
            (scrollView.contentOffset = .zero)
    }
    
    override func textFieldDidBeginEditing(_ textField:UITextField){
        super.textFieldDidBeginEditing(textField)
        guard let textField = textField.superview as? MDWTextFieldView else {
                currentTextField = nil
                return
        }
        currentTextField = textField
        refreshOffset()
    }
    
    override func textFieldDidEndEditing(_ textField:UITextField){
        super.textFieldDidEndEditing(textField)
        currentTextField = nil
    }
    
    override func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        return returnController?.textFieldShouldReturn(textField) ?? true
    }
    
}
