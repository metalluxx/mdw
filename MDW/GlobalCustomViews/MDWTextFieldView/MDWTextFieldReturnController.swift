//
//  MDWTextFieldReturnController.swift
//  MDW
//
//  Created by Metalluxx on 23/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

protocol MDWTextFieldReturnDelegte {
    var textFields:[MDWTextFieldView]? {get set}
    func textFieldShouldReturn(_ textField:UITextField) -> Bool
}

class MDWTextFieldReturnController:NSObject, MDWTextFieldReturnDelegte  {
    
    var textFields:[MDWTextFieldView]?
    deinit { textFields = nil }
    var alwaysEndEditing = false
    
    required init(withArray array:[MDWTextFieldView]) {
        self.textFields = array
    }
    
    convenience init(with animController:MDWTextFieldAnimationController) {
        self.init(withArray:animController.textFields)
    }
    
    convenience init(with animController:MDWTextFieldAnimationController, alwaysReturn:Bool) {
        self.init(with:animController)
        alwaysEndEditing = alwaysReturn
    }
    
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        var iterator  = makeIterator()
        while iterator.value?.tag != textField.tag {
            iterator.next()
        }
        
        if alwaysEndEditing  {
            iterator.value?.endEditing(true)
        } else {
        
        if let nextTextField = iterator.next() {
            guard let value = iterator.value else {
                nextTextField.endEditing(true);
                return false
            }
            value.fieldText.becomeFirstResponder()
        }
    }
        return true
    }
}


extension MDWTextFieldReturnController:Sequence {
    func makeIterator() -> MDWTextFieldIterator {
        return MDWTextFieldIterator(textFields:textFields!)
    }
}


struct MDWTextFieldIterator:IteratorProtocol {
    typealias Element = MDWTextFieldView
    private var textFields:[Element]
    var value:Element? { return textFields.first }
    
    init(textFields:[Element]) {
        self.textFields = textFields
    }
    
    @discardableResult mutating func next() -> Element? {
        defer {
            if !textFields.isEmpty { textFields.removeFirst() }
        }
        return textFields.first
    }
}
