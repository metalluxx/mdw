//
//  MDWTextFieldView.swift
//  MDW
//
//  Created by Metalluxx on 18/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class MDWTextFieldView:UIView {
    private(set) var fieldText = UITextField()
    private(set) var labelText = UILabel()
    private(set) var line = UIView()
    
    var animationController:MDWTextFieldAnimationController?{
        set{
            self.fieldText.delegate = newValue
        }
        get{
            return self.fieldText.delegate as? MDWTextFieldAnimationController
        }
    }
    
    override var tag:Int{
        didSet{
            fieldText.tag = tag
            labelText.tag = tag
        }
    }
    
    var lineColor:UIColor? {
        set{ line.backgroundColor = newValue }
        get{ return line.backgroundColor }
    }
    
    private var referenceFrame = CGRect.zero
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        
        line.backgroundColor = MDWConstant.TextFieldColors.defaultLine
        
        fieldText.placeholder = ""
        fieldText.clipsToBounds = false
        fieldText.keyboardType = .default
        fieldText.textColor = .black
        fieldText.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        fieldText.font = MDWConstant.boldFont(ofSize:24)

        labelText.text = ""
        labelText.font = MDWConstant.boldFont(ofSize:24)
        labelText.textColor = MDWConstant.TextFieldColors.label
        labelText.textAlignment = .left
        labelText.sizeToFit()
        
        tag = Int.random(in:Int.min...Int.max)
        
        line.translatesAutoresizingMaskIntoConstraints = false
        fieldText.translatesAutoresizingMaskIntoConstraints = false
        labelText.translatesAutoresizingMaskIntoConstraints = false
        
        insertSubview(line, at: 0)
        insertSubview(fieldText, at: 0)
        insertSubview(labelText, at: 1)
        
        NSLayoutConstraint.activate([
            line.bottomAnchor.constraint(equalTo:bottomAnchor),
            line.heightAnchor.constraint(equalToConstant:1),
            line.leftAnchor.constraint(equalTo:leftAnchor),
            line.rightAnchor.constraint(equalTo:rightAnchor),
            
            fieldText.bottomAnchor.constraint(equalTo:line.topAnchor, constant:-8),
            fieldText.heightAnchor.constraint(equalToConstant:24),
            fieldText.leftAnchor.constraint(equalTo:leftAnchor),
            fieldText.rightAnchor.constraint(equalTo:rightAnchor),
            
            labelText.topAnchor.constraint(equalTo:fieldText.topAnchor),
            labelText.leftAnchor.constraint(equalTo:fieldText.leftAnchor, constant:2),
            ])
    }
    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
    }
    
    func animateLabelToUp() -> Void {
        referenceFrame = labelText.frame
        UIView.animate(withDuration:0.2 ,
                       animations:{
                        self.labelText.transform = CGAffineTransform(scaleX:self.scaleMultiplier, y:self.scaleMultiplier)
        })
        UIView.animate(withDuration:0.2 ,
                       animations:{
                        self.labelText.frame.origin.x = 0
                        self.labelText.frame.origin.y -= 25
        })
    }
    
    func animateLabelToDown() -> Void {
        UIView.animate(withDuration:0.2 ,
                       animations:{
                        self.labelText.transform = CGAffineTransform(scaleX:self.scaleMultiplier, y:self.scaleMultiplier)
        })
        UIView.animate(withDuration:0.2 ,
                       animations:{
                        self.labelText.frame.origin.x = self.referenceFrame.origin.x
                        self.labelText.frame.origin.y = self.referenceFrame.origin.y
        })
    }

    
    private var scaleMultiplier:CGFloat {
        let ret:CGFloat = labelText.frame.size.height
        return 15 / ret
    }
}
