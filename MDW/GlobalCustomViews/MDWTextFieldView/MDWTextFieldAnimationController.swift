//
//  MDWTextFieldAnimationController.swift
//  MDW
//
//  Created by Metalluxx on 20/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class MDWTextFieldAnimationController:NSObject, UITextFieldDelegate {

    var textFields = [MDWTextFieldView]()
    var returnController:MDWTextFieldReturnController?
    
    func textFieldDidBeginEditing(_ textField:UITextField) {

        if (textField.text?.isEmpty)!  {
            for tfFromArray in textFields{
                if tfFromArray.fieldText.tag == textField.tag{
                    tfFromArray.animateLabelToUp()
                    break
                }
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField:UITextField) {
        
        if (textField.text?.isEmpty)!  {
            for tfFromArray in textFields{
                if tfFromArray.fieldText.tag == textField.tag{
                    tfFromArray.animateLabelToDown()
                    break
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        return returnController?.textFieldShouldReturn(textField) ?? true
    }
}
