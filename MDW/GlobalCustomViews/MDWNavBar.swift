//
//  MDWHelper.swift
//  MDW
//
//  Created by Metalluxx on 20/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import Foundation
import UIKit


class MDWNavBar:UIView {
    private(set) var label = UILabel()
    private(set) var separator = UIView()
    
    var labelText:String? {
        set{ label.text = newValue }
        get{ return label.text }
    }
    
    var separatorColor = MDWConstant.NavigationBarColors.separator{
        didSet{
            UIView.animate(withDuration:0.5) {
                switch self.isSeparated {
                case true:
                    self.separator.backgroundColor = self.separatorColor
                case false:
                    self.separator.backgroundColor = .clear
                }
            }
        }
    }
    
    var isSeparated:Bool = false {
        willSet{
            if isSeparated != newValue
        {
            UIView.animate(withDuration:0.5) {
                switch newValue {
                case true: self.separator.backgroundColor = self.separatorColor
                case false: self.separator.backgroundColor = .clear
                }
            }}
        }
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        initialSetupUI()
    }
    
    convenience init(title:String) {
        self.init()
        label.text = title
    }
    
    convenience init(viewController:UIViewController, title:String) {
        self.init(title:title)
        translatesAutoresizingMaskIntoConstraints = false
        viewController.view.addSubview(self)
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo:viewController.view.safeAreaLayoutGuide.topAnchor),
            leftAnchor.constraint(equalTo:viewController.view.leftAnchor),
            rightAnchor.constraint(equalTo:viewController.view.rightAnchor),
            heightAnchor.constraint(equalToConstant:60),
            ])
    }
    
    
    func initialSetupUI() -> Void {

        label.font = MDWConstant.boldFont(ofSize:24)
        label.textColor = MDWConstant.NavigationBarColors.title
        
        separator.backgroundColor = .clear
        
        label.translatesAutoresizingMaskIntoConstraints = false
        separator.translatesAutoresizingMaskIntoConstraints = false
        
        insertSubview(label, at: 0)
        insertSubview(separator, at: 0)
        
        NSLayoutConstraint.activate([
            label.bottomAnchor.constraint(equalTo:bottomAnchor, constant:-17),
            label.leftAnchor.constraint(equalTo:leftAnchor, constant:20),
            label.rightAnchor.constraint(equalTo:rightAnchor, constant:-20),
            
            separator.heightAnchor.constraint(equalToConstant:1),
            separator.bottomAnchor.constraint(equalTo:bottomAnchor),
            separator.leftAnchor.constraint(equalTo:leftAnchor),
            separator.rightAnchor.constraint(equalTo:rightAnchor),
            ])
    }
    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
    }
}


class MDWNavBarWithButton:MDWNavBar {
    
    private(set) var button = UIButton(type:.system)
    
    var buttonText:String?{
        get{ return button.currentTitle }
        set{ button.setTitle(newValue, for:.normal) }
    }
    
    convenience init(viewController:UIViewController, title:String , buttonText:String, target:Any? , selector:Selector?){
        self.init(viewController:viewController, title:title)
        
        button.backgroundColor = .clear
        button.setTitle(buttonText, for:.normal)
        selector != nil ? button.addTarget(target, action:selector!, for:.touchUpInside):nil
        button.contentHorizontalAlignment = .right
        button.titleLabel?.font = UIFont(name:"ApercuPro-Regular", size:17)
        button.setTitleColor(MDWConstant.NavigationBarColors.buttonText , for:.normal)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        insertSubview(button, at: 0)
        
        NSLayoutConstraint.activate([
            button.rightAnchor.constraint(equalTo:label.rightAnchor),
            button.centerYAnchor.constraint(equalTo:label.centerYAnchor),
            button.heightAnchor.constraint(equalToConstant:button.intrinsicContentSize.height),
            button.widthAnchor.constraint(equalToConstant:button.intrinsicContentSize.width),
            ])

    }
}

