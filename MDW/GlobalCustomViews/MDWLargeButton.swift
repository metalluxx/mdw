//
//  InfoButtonCell.swift
//  MDW
//
//  Created by Metalluxx on 18/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class MDWLargeButton:UIView {

    private var label = UILabel()
    private var tapTouch = UITapGestureRecognizer(target:self, action:#selector(tappedButton))
    
    var tapHandler:(() -> Void)?
    
    var titleText:String?{
        set{ label.text = newValue }
        get{ return label.text }
    }

    override init(frame:CGRect) {
        super.init(frame:frame)

        backgroundColor = MDWConstant.GrayButtonBackColor
        addGestureRecognizer(tapTouch)
        
        label.text = ""
        label.font = MDWConstant.normalFont(ofSize:17)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo:topAnchor, constant:10),
            label.leftAnchor.constraint(equalTo:leftAnchor, constant:10),
            label.rightAnchor.constraint(equalTo:rightAnchor, constant:-10),
            label.heightAnchor.constraint(equalToConstant:20),
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func tappedButton(_ sender:UITapGestureRecognizer) -> Void {
        tapHandler?()
    }
}
