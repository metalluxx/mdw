//
//  AcceptLoginViewController.swift
//  MDW
//
//  Created by Metalluxx on 06/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

class AcceptLoginViewController:UIViewController {

    private var loginButton = UIButton()
    private var effectView = UIVisualEffectView(effect:UIBlurEffect(style:.extraLight))
    private var label = UILabel()
    
    var buttonHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label.textColor = .black
        label.numberOfLines = 0
        label.font = MDWConstant.normalFont(ofSize:17)
        label.text = NSLocalizedString("ALVC_LABEL_TEXT", comment:"")
        
        loginButton.backgroundColor = .black
        loginButton.titleLabel?.textColor = .white
        loginButton.setTitle(NSLocalizedString("LOGIN_BUTTON", comment:""), for:.normal)
        loginButton.titleLabel?.font = MDWConstant.normalFont(ofSize:17)
        loginButton.addTarget(self, action:#selector(tappedLoginButton), for:.touchUpInside)
        
        effectView.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(effectView)
        effectView.contentView.insertSubview(label, at: 0)
        effectView.contentView.insertSubview(loginButton, at: 0)
        
        NSLayoutConstraint.activate([
            effectView.topAnchor.constraint(equalTo:view.topAnchor),
            effectView.bottomAnchor.constraint(equalTo:view.bottomAnchor),
            effectView.leftAnchor.constraint(equalTo:view.leftAnchor),
            effectView.rightAnchor.constraint(equalTo:view.rightAnchor),
            
            label.centerXAnchor.constraint(equalTo:effectView.contentView.centerXAnchor),
            label.centerYAnchor.constraint(equalTo:effectView.contentView.centerYAnchor),
            label.leftAnchor.constraint(equalTo:effectView.contentView.leftAnchor, constant:20),
            label.rightAnchor.constraint(equalTo:effectView.contentView.rightAnchor, constant:-20),
            
            loginButton.bottomAnchor.constraint(equalTo:effectView.contentView.bottomAnchor, constant:-20),
            loginButton.leftAnchor.constraint(equalTo:effectView.contentView.leftAnchor, constant:20),
            loginButton.rightAnchor.constraint(equalTo:effectView.contentView.rightAnchor, constant:-20),
            loginButton.heightAnchor.constraint(equalToConstant:60)
            ])
    }

    @objc func tappedLoginButton() -> Void {
        buttonHandler?()
    }
}
