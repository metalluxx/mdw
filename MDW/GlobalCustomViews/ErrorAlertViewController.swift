//
//  MDWErrorViewController.swift
//  MDW
//
//  Created by Metalluxx on 03/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit


extension UIAlertController {
    convenience init(error:String?) {
        self.init(title:NSLocalizedString("ERROR", comment:""), message:error ?? "" , preferredStyle:.alert)
        let aletrButton = UIAlertAction.init(title:"Ok", style:.destructive)
        self.addAction(aletrButton)
    }
}


