
//
//  ErrorAlertNotificationView.swift
//  MDW
//
//  Created by Metalluxx on 06/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

enum MDWAlertNotificationType  {
    case error, alert
}


extension MDWAlertNotificationType {
    var getColor:UIColor {
        switch self {
        case .alert:
            return .green
        case .error:
            return .init(red:1, green:87/255, blue:48/255, alpha:1)
        }
    }
}

class ErrorAlertNotificationView:UIView {
    var informationString:String?
    var informationType:MDWAlertNotificationType?
    
    var label = UILabel()
    
    required init(_ info:String, type:MDWAlertNotificationType) {
        super.init(frame:CGRect.zero)

        backgroundColor = .clear
        label.layer.cornerRadius = 10
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 15
        layer.shadowOpacity = 0.8
        
        label.font = MDWConstant.normalFont(ofSize: 13)
        label.textColor = .white
        label.textAlignment = .center
        
        label.translatesAutoresizingMaskIntoConstraints = false
        translatesAutoresizingMaskIntoConstraints = false
        insertSubview(label, at: 0)
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            label.leftAnchor.constraint(equalTo: leftAnchor, constant: 10),
            label.rightAnchor.constraint(equalTo: rightAnchor, constant: -10),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            ])
        setErrorInformation(info, type:type)
    }
    
    func setErrorInformation(_ info:String, type:MDWAlertNotificationType) -> Void {
        label.backgroundColor = type.getColor
        label.text = info
    }

    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
    }
    
    @discardableResult static func showingAlert(in view:UIView, info:String, type:MDWAlertNotificationType) -> ErrorAlertNotificationView {
        
        let vc = ErrorAlertNotificationView(info, type:type)
        
        UIView.transition(with:view, duration:0.25, options:.transitionCrossDissolve, animations:{
            view.addSubview(vc)
        })
        
        NSLayoutConstraint.activate([
            vc.topAnchor.constraint(equalTo:view.layoutMarginsGuide.topAnchor),
            vc.heightAnchor.constraint(equalToConstant:60),
            vc.leftAnchor.constraint(equalTo:view.leftAnchor),
            vc.rightAnchor.constraint(equalTo:view.rightAnchor),
            ])
        
        DispatchQueue.global().asyncAfter(deadline:.now() + 5.0 )  {
            DispatchQueue.main.async {
                UIView.transition(with:view, duration:0.25, options:.transitionCrossDissolve, animations:{
                    vc.removeFromSuperview()
                })
            }
        }
        return vc
    }

}
