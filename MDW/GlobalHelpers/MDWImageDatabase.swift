//
//  MDWImageDatabase.swift
//  MDW
//
//  Created by Metalluxx on 31/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

protocol MDWImageCacheProtocol {
    var cache:NSCache<NSString,UIImage> {get set}
    func getImageFromCache(string:String) -> UIImage?
    func storeImageInCache(image:UIImage,string:String)
}


extension MDWImageCacheProtocol{
    func getImageFromCache(string:String) -> UIImage? {
        return cache.object(forKey: NSString(string:string))
    }
    func storeImageInCache(image:UIImage,string:String){
        cache.setObject(image, forKey: NSString(string:string))
    }
}

