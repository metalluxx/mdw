//
//  AppRouter.swift
//  MDW
//
//  Created by Metalluxx on 24/03/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

enum SMAuthStatus{case none, user, guest}

class SessionManager:NSObject {
    static let shared = SessionManager()
    var networkManager = NetworkManager()
    
    var sessionStatus = SMAuthStatus.none
    var sessionToken:MDWAuthToken?
}

