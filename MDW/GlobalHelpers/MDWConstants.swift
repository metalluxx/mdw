//
//  MDWConstants.swift
//  MDW
//
//  Created by Metalluxx on 01/04/2019.
//  Copyright © 2019 Metalluxx. All rights reserved.
//

import UIKit

struct MDWConstant {
    
    struct TextFieldColors {
        static let defaultLine = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1)
        static let errorLine = UIColor(red:0.92, green:0.34, blue:0.34, alpha:1)
        static let label = UIColor(red:0.6, green:0.6, blue:0.6, alpha:1)
    }
    
    struct LVCTextFieldColors {
        static let unfocus = UIColor(red:0.6, green:0.6, blue:0.6, alpha:1)
        static let focus = UIColor.black
    }
    
    struct ImageHeader {
        static let textColor = UIColor.white
        static let height = CGFloat(200.0)
    }
    
    struct NavigationBarColors {
        static let separator = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1)
        static let title = UIColor(red:0.89, green:0.2, blue:0.14, alpha:1)
        static let buttonText = UIColor(red:0.6, green:0.6, blue:0.6, alpha:1)
    }
    
    
    static let OffsetForShowingSeparator = CGFloat(10.0)
    static let ProjectTableCellHeigth = CGFloat(300.0)
    static let NewsImageCellHeight = CGFloat(200.0)

    static let GrayButtonBackColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1)

    static func normalFont(ofSize:CGFloat) -> UIFont{
        return UIFont(name:"ApercuPro-Regular", size:ofSize) ?? UIFont.systemFont(ofSize:ofSize)
    }
    static func boldFont(ofSize:CGFloat) -> UIFont{
        return UIFont(name:"ApercuPro-Bold", size:ofSize) ?? UIFont.boldSystemFont(ofSize:ofSize)
    }
}


